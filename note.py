	# The outer discussion/ root directory is a container for your project. Its name doesn’t matter to Django; you can rename it to anything you like.
	# manage.py: A command-line utility that lets you interact with this Django project in various ways.
	# The inner discussion/ directory is the actual Python package for your project. Its name is the Python package name you’ll need to use to import anything inside it (e.g. mysite.urls).
	# discussion/__init__.py: An empty file that tells Python that t
	# discussion/__init__.py: An empty file that tells Python that this directory should be considered a Python package.
	# discussion/settings.py: Settings/configuration for this Django project.
	# discussion/urls.py: The URL declarations for this Django project; a “table of contents” of your Django-powered site.
	# discussion/asgi.py: An entry-point for ASGI-compatible web servers to serve your project.
	# discussion/wsgi.py: An entry-point for WSGI-compatible web servers to serve your project.

# Creating Setup on Django
# django-admin startproject project_name
# python manage.py startapp app_name
# python manage.py runserver


# making migration
# python manage.py makemigrations app_filename

# creating table command
# python manage.py sqlmigrate app_filename migration_name

# migrate command
# python manage.py migrate

# opening shell in python
# python manage.py shell

# from todolist.models import ToDoItem - importing models using shell
# ToDoItem.objects.all() - retrieving data from model
# from django.utils import timezone - importing timezone

# creating data and save it
# variable name     model class name
# todoitem      =     ToDoItem         (task_name="Eat", description="Dinner time, order pizza with extra cheese", date_created=timezone.now())
# todoitem.save()

# 1. Create a GroceryItem model with the following fields:
# - item_name - CharField and max_length = 50
# - category - CharField and max_length = 50
# - status - CharField, max_length = 50 and default value of pending
# - date_created DateTimeField
# 2. Include the django_practice app in the installed apps.
# 3. Run the migration command to inform the Django app that changes have been made to the GroceryItem (e.g. python manage.py makemigrations django_practice).
# 4. Run the command to manage the migrations for out django_practice app (e.g. python manage.py sqlmigrate django_practice 0001).
# 5. Check for any changes in the migrations to apply them to our database (e.g. python manage.py migrate).
# 6. Install mysqlclient.
# - pip install mysql_connector
# - pip install mysql
# 7. Change the database to connect to the MySQL database instead of the SQLite
# 8. Create a new database named django_db.
# 9. Check for any changes in the migrations to apply them to our database.
# - python manage.py makemigrations django_practice
# - python manage.py migrate
# 10. Create a Django Admin superuser.
# - macOS/linux users - python manage.py createsuperuser
# - windows users- winpty python manage.py createsuperuser
# 11. Make the django_practice app modifiable via Django admin.
# 12. Refactor the index view to use the shortened syntax utilizing the render method.
# 13. Add a templates folder, a django_practice folder inside of it and finally an index.html file inside it to implement the use of Django's template system to integrate dynamic data and use HTML elements.


# shell command
# >>> from todolist.models import ToDoItem
# >>> ToDoItem.objects.all()
# >>> from django.utils import timezone  
# >>> todoitem = ToDoItem(task_name = "Eat", description = "Dinner time, order burgir, pries, and relyenong dilis", date_created = timezone.now())  
# >>> todoitem.save()

# creating admin command
# python manage.py createsuperuser