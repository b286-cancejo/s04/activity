from django.shortcuts import render, redirect, get_object_or_404

from django.http import HttpResponse
from .models import ToDoItem, Events
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from .forms import LoginForm, AddTaskForm, AddUser, AddEvent
from django.contrib.auth.hashers import make_password

# Create your views here.

def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    eventitem_list = Events.objects.filter(user_id = request.user.id)
    context = {
        'todoitem_list': todoitem_list,
        'eventitem_list': eventitem_list,
        "user" : request.user
        }
    return render(request, "todolist/index.html", context)
    # return HttpResponse("Hello from the views.py file")

def todoitem(request, todoitem_id):
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    # response = "You are viewing the details of %s"
    # return HttpResponse(response %todoitem_id)
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def eventitem(request, eventitem_id):
    eventitem = get_object_or_404(Events, pk=eventitem_id)
    # response = "You are viewing the details of %s"
    # return HttpResponse(response %todoitem_id)
    return render(request, "todolist/eventitem.html", model_to_dict(eventitem))

def register(request):
    # users = User.objects.all()
    # is_user_registered = False
    # context = {
    #     "is_user_registered" : is_user_registered
    # }

    # # check if the username "johndoe" already exist
    # # if it exist change the value of variable is_user_registered to True
    # for indiv_user in users:
    #     if indiv_user.username == "johndoe":
    #         is_user_registered = True
    #         break
    
    # if is_user_registered == False:
    #     user = User()
    #     user.username = "johndoe"
    #     user.first_name = "John"
    #     user.last_name = "Doe"
    #     user.email = "john@mail.com"
    #     # The set_password is used to ensure that the password is hashed using Django's authentication framework
    #     user.set_password("John1234")
    #     user.is_staff = False
    #     user.is_active = True
    #     user.save()

    # context = {
    #     "first_name" : indiv_user.first_name,
    #     "last_name" : indiv_user.last_name
    # }

    context = {}

    if request.method == "POST":
        form = AddUser(request.POST)
        if form.is_valid() == False:
            form = AddUser()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            duplicate = User.objects.filter(username = username)

            if not duplicate:
                User.objects.create(username=username, password=make_password(password),first_name=first_name, last_name=last_name, email=email, is_staff=False,is_active=True)
                return redirect("todolist:index")
            
            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/register.html", context)

def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="johndoe", password="John1234")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')# this is where we get the user with username 'John Doe'
        authenticated_user.set_password("johndoe1") # This is where we change the password
        authenticated_user.save()
        is_user_authenticated = True
    
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "todolist/change_password.html", context)

def login_view(request):
    context = {}

    if request.method == "POST":
        # Create a form instance and populate it with data from the request
        form=LoginForm(request.POST)

        if form.is_valid() == False:
            # Return a blank login form
            form = LoginForm()
        else:
            # receives the information from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }
            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/login.html", context)

    # username = "johndoe"
    # password = "johndoe1"
    # user = authenticate(username=username, password=password)

    # context={
    #     "is_user_authenticated": False
    # }
    # print(user)
    # if user is not None:
    #     login(request, user)
    #     return redirect("todolist:index")
    
    # else:
    #     return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):
    context = {}

    if request.method == "POST":
        form = AddTaskForm(request.POST)
        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            duplicate = ToDoItem.objects.filter(task_name = task_name)

            if not duplicate:
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id = request.user.id)
                return redirect("todolist:index")
            
            else:
                context = {
                    "error": True
                }
    return render(request, "todolist/add_task.html", context)

def add_events(request):

    context = {}
    
    if request.method == "POST":
        form = AddEvent(request.POST)
        if form.is_valid() == False:
            form = AddEvent()
        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']
            duplicate = Events.objects.filter(event_name = event_name)

            if not duplicate:
                Events.objects.create(event_name=event_name, description=description, status=status, event_date=timezone.now(), user_id = request.user.id)
                return redirect("todolist:index")
            
            else:
                context = {
                    "error": True
                }
                
    return render(request, "todolist/add_events.html", context)



